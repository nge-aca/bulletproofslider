"""
Django-specific dispatcher runner
"""
from bulletproofslider.dispatcher import Dispatcher
from django.conf import settings

disp = Dispatcher()
disp.queue_url = settings.BULLETPROOFSLIDER_REPLY_QUEUE

if __name__ == '__main__':
    disp.run()
