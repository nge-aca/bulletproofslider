import boto3
import qbxml
from ..utils import import_string
from .. import QBCONFIG, Message
import logging
try:
    import lxml.etree as etree
except ImportError:
    import xml.etree.ElementTree as etree


class Dispatcher:
    """
    Generic class to do the dispatching
    """
    @property
    def queue_url(self):
        return self._queue_url

    @queue_url.setter
    def queue_url(self, val):
        self._queue_url = val
        if val is None:
            self.queue = None
        else:
            self.queue = self.sqs.Queue(val)

    def __init__(self):
        self.sqs = boto3.resource('sqs')
        self._queue_url = None
        self.queue = None
        self.log = logging.getLogger("bulletproofslider.dispatcher")

    def run(self):
        """
        Process replies forever. Or until SIGKILL.
        """
        while True:
            self.run_once()

    @staticmethod
    def _splitbody(body):
        yield from etree.fromstring(
            "<msg>{}</msg>".format(body)
        )

    @staticmethod
    def _reduce_attrs(mattrs):
        rv = {}
        for key, info in mattrs.items():
            # TODO: Have a type registration system?
            typ, *_ = info['DataType'].split('.', 1)
            if typ == 'String':
                rv[key] = info['StringValue']
            elif typ == 'Number':
                if '.' in info['StringValue']:
                    rv[key] = float(info['StringValue'])
                else:
                    rv[key] = int(info['StringValue'])
            elif typ == 'Binary':
                rv[key] = info['BinaryValue']

        return rv

    def run_once(self):
        """
        Queries SQS for messages once and processes them.

        Note: Uses long polling, so may not be applicable for general-purpose async.
        """
        # Set these as high as SQS will allow you.
        for message in self.queue.receive_messages(MaxNumberOfMessages=10,
                                                   WaitTimeSeconds=20,
                                                   MessageAttributeNames=['All']):
            if not message:
                continue

            # Only using next() here because we should be passing one element at a time
            resps = [next(qbxml.parse(x, QBCONFIG)) for x in self._splitbody(message.body)]
            kwargs = self._reduce_attrs(message.message_attributes)

            try:
                qname = kwargs[Message.CALLBACK_ATTR]
            except KeyError:
                self.log.warning("No callback in %r", kwargs)
            else:
                del kwargs[Message.CALLBACK_ATTR]
                del kwargs['replyQueue']
                
                callback = import_string(qname)
                self.call(callback, resps, **kwargs)

                message.delete()

    def call(self, callback, resps, **kwargs):
        """
        Actually calls the callback. Mostly exists for overriding.
        """
        try:
            callback(resps, **kwargs)
        except Exception:
            self.log.exception("Error calling callback")
