import sys
from pprint import pprint
import threading
from bulletproofslider import DeckBoom
from bulletproofslider.dispatcher import Dispatcher
import qbxml.fields
import random

if not (2 <= len(sys.argv) <= 3):
    sys.exit("Syntax: bulletproofslider <send queue> [<reply queue>]")
if len(sys.argv) >= 3 and sys.argv[1] == sys.argv[2]:
    sys.exit("Don't use the same queue for requests and replies")

qb = DeckBoom()
dispatch = Dispatcher()

qb.send_queue = sys.argv[1]
if len(sys.argv) >= 3:
    replyq = sys.argv[2]
else:
    import boto3
    import atexit
    sqs = boto3.client('sqs')
    name = 'bulletproofslider-interactive-shell-{}'.format(random.randint(1, 999))
    q = sqs.create_queue(
        QueueName=name,
        Attributes={
            'Policy': """{
                "Version": "2012-10-17",
                "Id": "arn:aws:sqs:us-east-1:757385407765:qbm-dev-reply/SQSDefaultPolicy",
                "Statement": [
                    {
                      "Sid": "Sid1472232432532",
                      "Effect": "Allow",
                      "Principal": "*",
                      "Action": [
                        "SQS:SendMessage",
                        "SQS:ReceiveMessage",
                        "SQS:DeleteMessage",
                        "SQS:GetQueueAttributes",
                        "SQS:GetQueueUrl"
                      ],
                      "Resource": "arn:aws:sqs:us-east-1:*:%s"
                    }
                ]
            }""" % name
        },
    )

    replyq = q['QueueUrl']

    atexit.register(sqs.delete_queue, QueueUrl=replyq)


qb.reply_queue = dispatch.queue_url = replyq

def _dump(name, obj, indent, **pops):
    print(indent + name + ':', **pops)
    indent += ' ' * 4
    for name, val in vars(obj).items():
        if name.startswith('_') or name == 'config':
            continue
        if isinstance(val, qbxml.fields.Aggregate):
            _dump(name, val, indent, **pops)
        elif isinstance(val, list):
            print('{}{}\t['.format(indent, name, repr(val)))
            first = True
            for obj in val:
                _dump(name, obj, indent + ' ' * 4, **pops)
            print('{}]'.format(indent, name, repr(val)))
        else:
            print('{}{}\t{}'.format(indent, name, repr(val)))

def dump(resp, **kwargs):
    print("Received response:")
    for obj in resp:
        _dump(type(obj).__name__, obj, '')
    pprint(kwargs)

lcls = {
    'qb': qb,
    'dump': dump,
} 

dispatch_thread = threading.Thread(name='dispatch', target=dispatch.run, daemon=True)
dispatch_thread.start()

banner = """
DeckBoom instance available as `qb`.
Use `dump` to just print replies.
Dispatcher running in background.
"""
try:
    import IPython
except ImportError:
    import code
    code.interact(banner=banner, local=lcls)
else:
    print(banner)
    IPython.start_ipython(user_ns=lcls, argv=[])