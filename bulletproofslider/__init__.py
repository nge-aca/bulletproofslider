import qbxml
import qbxml.classes
import boto3
try:
    import lxml.etree as etree
except ImportError:
    import xml.etree.ElementTree as etree

__all__ = 'DeckBoom',

QBCONFIG = qbxml.QBXMLConfig(qbxml.QBXMLConfig.Desktop_Edition)


class DeckBoom:
    """
    Basic class to talk to DeckBoom

    with DeckBoom().send(mycall) as msg:
        ...
    """
    def __init__(self):
        self.reply_queue = None
        self.send_queue = None

    @classmethod
    def from_settings(cls, mapping):
        self = cls()
        vars(self).update(mapping)
        return self

    def send(self, reply=None, **attrs):
        """
        Context manager that takes the queue the reply should be sent to
        (string) and name-value pairs that will be sent with the reply message.

        Note that the values must be serializable according to the sqs code.
        """
        rname = reply.__qualname__ if reply is not None else None
        return Message(self, rname, **attrs)


class Message:
    """
    A single DeckBoom "transaction" (even though DeckBoom makes no
    transactional guarentees of any kind).

    In addition to being a context manager (see DeckBoom above), there are 
    constructor methods for every request type in qbXML, named as just the
    basename, eg QueryHost. These methods return the appropriate object from
    the qbxml and will be sent when the context manager exits successfully.
    """
    CALLBACK_ATTR = 'BulletproofSliderCallback'
    def __init__(self, db, callback, **attrs):
        self.deckboom = db
        self.callback = callback
        self.attributes = attrs
        self._tags = []
        self.reply_queue = self.deckboom.reply_queue
        self.send_queue = None
        if self.deckboom.send_queue:
            self.send_queue = boto3.resource('sqs').Queue(self.deckboom.send_queue)

    def __enter__(self):
        return self

    def __exit__(self, et, ev, etb):
        if et:
            # Do not send if there was an error
            return
        msg = "".join(etree.tostring(t.to_xml()).decode('utf-8') for t in self._tags)
        attrs = self._buildattrs()
        if self.send_queue is not None:
            self.send_queue.send_message(MessageBody=msg, MessageAttributes=attrs)
        else:
            print("No queue for {}".format(self))

    def _buildattrs(self):
        rv = {
            self.CALLBACK_ATTR: {
                'DataType': 'String.QualName',
                'StringValue': self.callback
            },
            'replyQueue': {
                'DataType': 'String.URL',
                'StringValue': self.reply_queue,
            },
        }
        for k, v in self.attributes.items():
            if isinstance(v, numbers.Real):
                rv[k] = {
                    'DataType': 'Number.{}'.format(type(v).__qualname__),
                    'StringValue': str(v),
                }
            elif isinstance(v, str):
                rv[k] = {
                    'DataType': 'String',
                    'StringValue': v,
                }
            elif isinstance(v, bytes):
                rv[k] = {
                    'DataType': 'Binary',
                    'BinaryValue': v,
                }
            else:
                # TODO: Have a type registration system?
                raise ValueError("Can't handle type {} on SQS attribute {}".format(type(v), k))
        return rv

    def __getattr__(self, name):
        cls = getattr(qbxml.classes, name+'Rq')  # Will probably be a LazyObject :(
        def func(self, **attrs):
            msg = cls(config=QBCONFIG)
            self._tags.append(msg)
            for k, v in attrs.items():
                setattr(msg, k, v)
            return msg
        func.__name__ = name
        func.__qualname__ = type(self).__name__ + '.' + func.__name__
        setattr(type(self), name, func)  # Hopefully we won't be called again for this
        return func.__get__(self)

    # Sadly, I think for this to work on the class level, we'd need a metaclass
    def __dir__(self):
        # Add the lazy-created methods
        return set(super().__dir__()) | {n[:-2] for n in dir(qbxml.classes) if n.endswith('Rq')}
