from importlib import import_module

# Blatently stolen from django.utils.module_loading
def import_string(dotted_path):
    """
    Import a dotted module path and return the attribute/class designated by the
    last name in the path. Raise ImportError if the import failed.
    """
    if '.' in dotted_path:
        module_path, class_name = dotted_path.rsplit('.', 1)
    else:
        module_path, class_name = '__main__', dotted_path

    module = import_module(module_path)

    try:
        return getattr(module, class_name)
    except AttributeError:
        msg = 'Module "%s" does not define a "%s" attribute/class' % (
            module_path, class_name)
        raise  ImportError(msg)
