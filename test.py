import bulletproofslider as bps

qb = bps.DeckBoom()

with qb.send('myqueue', spam='egg') as msg:
    msg.InvoiceAdd({'InvoiceAdd': {
        '$defMacro': 'TxnID:SpamEggs',
        'CustomerRef': {'FullName': 'Sam'},
        'BillAddress': "123",
        'MultiTag': [
            "foo",
            "bar",
        ],
    }})