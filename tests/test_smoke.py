import pytest
import bulletproofslider as bps


@pytest.fixture
def deckboom():
    return bps.DeckBoom()


def test_smoke_employeemod(deckboom):
    with deckboom.send(None) as msg:
        rq = msg.EmployeeMod()
        rq.requestID = '12345'
        rq.EmployeeMod.ListID = 'employee-list-id'

        assert len(msg._tags) == 1
        assert type(msg._tags[0]).__name__ == 'EmployeeModRq'


def test_smoke_invoiceadd(deckboom):
    with deckboom.send(None) as msg:
        rq = msg.InvoiceAdd()
        rq.InvoiceAdd.defMacro = 'TxnID:SpamEggs'
        rq.InvoiceAdd.CustomerRef.FullName = 'Sam'
        rq.InvoiceAdd.BillAddress.Addr1 = "123"
        # TODO: check list

        assert len(msg._tags) == 1
        assert type(msg._tags[0]).__name__ == 'InvoiceAddRq'
