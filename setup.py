from setuptools import setup

setup(
    name='bulletproofslider',
    version='0.1dev',
    packages=['bulletproofslider'],
    license='LGPL',
    long_description=open('README.rst').read(),
    install_requires=[
        'qbxml',
        'boto3'
    ],
)
