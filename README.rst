==================
Bulletproof Slider
==================

This is the companion library to `django-deckboom`_. It provides utilities for connecting through SQS, working with qbXML, and dispatching.

::

    import bulletproofslider as bs

    qb = bs.from_settings(settings)

    with qb.send(mycallback, customer=c.id) as msg:
        rq = msg.InvoiceAdd()
        rq.InvoiceAdd.defMacro = 'TxnID:SpamEggs'
        rq.InvoiceAdd.CustomerRef.FullName = 'Sam'
        rq.InvoiceAdd.BillAddress.Addr1 = "123"


For more information on qbXML, see the `Intuit On-Screen Reference`_ (Only runs on Firefox and old browsers.)

.. _django-deckboom: https://bitbucket.org/nge-aca/django-deckboom
.. _Intuit On-Screen Reference: https://developer-static.intuit.com/qbsdk-current/common/newosr/index.html

Getting Results
---------------
Unfortunately, all job queue libraries I've seen fail horribly in this use case (ie, to support it would require significant work and insider knowledge, with no guarentees of maintainability), so Bulletproof Slider has its own dispatch system.

BPS dispatch doesn't require registration or anything. It just requires globally-accessible callable (ie, something with a ``__qualname__``).

The callback is called with a list of responses and the keyword arguments passed to ``qb.send()``.
